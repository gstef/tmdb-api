package com.zenika.academy.tmdb;

import java.time.Year;
import java.util.Collections;
import java.util.List;

public class MovieInfo {
    public final String title;
    public final Year year;
    public final List<Character> cast;

    public MovieInfo(String title, Year year, List<Character> cast) {
        this.title = title;
        this.year = year;
        this.cast = Collections.unmodifiableList(cast);
    }

    public String toString() {
        return this.title + " - " + this.year.toString();
    }

    public static class Character {
        public final String characterName;
        public final String actorName;

        public Character(String characterName, String actorName) {
            this.characterName = characterName;
            this.actorName = actorName;
        }
    }
}
